package tempoemfalta;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

//@author flaviosilva

public class Contas {
    
    public void comantariosTestesErrados(){
        /*
        // Calcula a diferença de meses entre as duas datas
        long diferencaEmMes = ChronoUnit.MONTHS.between(hoje, outraData);
        // Calcula a diferença de anos entre as duas datas
        long diferencaEmAnos = ChronoUnit.YEARS.between(hoje, outraData);
        
        // Exibe a diferença em dias entre as datas
        System.out.println("Diferença em dias entre " + hoje + " e " + outraData + " = " + diferencaEmDias);
        // Exibe a diferença em meses entre as datas
        System.out.println("Diferença em meses entre " + hoje + " e " + outraData + " = " + diferencaEmMes);
        // Exibe a diferença em anos entre as datas
        System.out.println("Diferença em anos entre " + hoje + " e " + outraData + " = " + diferencaEmAnos);
        */
    }
    
    public static String mostrarDiferencaDeTempo(Month mes ,int dia, int mesInt){
        LocalDate hoje = LocalDate.now(); 
        Calendar cal = Calendar.getInstance();
        int anoAtual = cal.get(Calendar.YEAR);
        
        LocalDate data = LocalDate.of(anoAtual, mes, dia);
        long diferencaEmDias = ChronoUnit.DAYS.between(hoje, data);
        
        
       if (diferencaEmDias < 0) {
           anoAtual+=1;
            data = LocalDate.of(anoAtual, mes, dia);
            diferencaEmDias = ChronoUnit.DAYS.between(hoje, data);
        }
       
        int ano0 = anoAtual - (14 - mesInt) / 12;
        int num1 = ano0 + ano0/4 - ano0/100 + ano0/400;
        int mes0 = mesInt + 12 * ((14 - mesInt) / 12) - 2;
        int diaD = (dia + num1 + (31*mes0)/12) % 7;
        
        // ------------Dia da Semana---------------
       String diaSemana="";
        if (diaD==1) {
            diaSemana="Segunda-feira:";
        }
        else if (diaD==2){
            diaSemana="Terça-feira:";
        }
        else if (diaD==3){
            diaSemana="Quarta-feira:";
        }
        else if (diaD==4){
            diaSemana="Quinta-feira:";
        }
        else if (diaD==5){
            diaSemana="Sexta-feira:";
        }
        else if (diaD==6){
            diaSemana="Sabado:";
        }
        else{
            diaSemana="Domingo:";
        }//End else
       
        return diaSemana +" " + Long.toString(diferencaEmDias);
}
    
    public static String mostrarCalcularDia(int dia, int mesInt, Month mesMonth, int anoAtual){
        
        LocalDate hoje = LocalDate.now(); 
        Calendar cal = Calendar.getInstance();
        
        LocalDate data = LocalDate.of(anoAtual, mesMonth, dia);
        long diferencaEmDias = ChronoUnit.DAYS.between(hoje, data);
        
        
        int ano0 = anoAtual - (14 - mesInt) / 12;
        int num1 = ano0 + ano0/4 - ano0/100 + ano0/400;
        int mes0 = mesInt + 12 * ((14 - mesInt) / 12) - 2;
        int diaD = (dia + num1 + (31*mes0)/12) % 7;
        
        // ------------Dia da Semana---------------
       String diaSemana="";
        if (diaD==1) {
            diaSemana="Segunda-feira!";
        }
        else if (diaD==2){
            diaSemana="Terça-feira!";
        }
        else if (diaD==3){
            diaSemana="Quarta-feira!";
        }
        else if (diaD==4){
            diaSemana="Quinta-feira!";
        }
        else if (diaD==5){
            diaSemana="Sexta-feira!";
        }
        else if (diaD==6){
            diaSemana="Sabado!";
        }
        else{
            diaSemana="Domingo!";
        }//End else
       
        return diaSemana +" " + Long.toString(diferencaEmDias);
    }
    
}
